<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create([
            'name' => 'Super Admin',
            'slug' => 'super-admin',
            'permissions' => json_encode([
                'admin.super-admin' => true
            ])
        ]);

        Role::create([
            'name' => 'Poster',
            'slug' => 'poster',
            'permissions' => json_encode( [
                'category.view' => true,
                'category.create' => true,
                'category.update' => true,
                'category.delete' => true
            ])
        ]);
        Role::create([
            'name' => 'admin',
            'slug' => 'admin',
            'permissions' => json_encode([
                'tag.view' => true,
                'tag.create' => true,
                'tag.update' => true,
                'tag.delete' => true
            ])
        ]);

    }
}
