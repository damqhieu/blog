<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
          App\User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin@admin.com')
        ]);

        \App\User::create([
            'name' => 'Category',
            'email' => 'admin1@admin.com',
            'password' => bcrypt('admin@admin.com'),
        ]);

        \App\User::create([
            'name' => 'Tag' ,
            'email' => 'admin2@admin.com',
            'password' => bcrypt('admin@admin.com')

        ]);
        \App\User::create([
            'name' => 'Article' ,
            'email' => 'admin3@admin.com',
            'password' => bcrypt('admin@admin.com')
        ]);

    }
}
