<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; $i++) {
            Category::create([
                'name' => $faker->text(30),
                'slug' => $faker->slug('title'),
                'description' => $faker->text(20),
                'content' => $faker->text(20)
            ]);
        }
    }
}
