<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Article::truncate();
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            \App\Article::create([
                'category_id' => rand(1,10),
                'title' => $faker->text(191),
                'slug' => $faker->slug('title'),
                'author' => $faker->name,
                'description' => $faker->text(191),
                'content' => $faker->text(191)
            ]);
        }
    }
}
