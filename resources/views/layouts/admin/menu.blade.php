<li class="header">MAIN NAVIGATION</li>
<li class=" treeview">
    <a href="{{ route('categories.index') }}">
        <i class="fa fa-dashboard"></i> <span>Category</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="active"><a href="{{ route('categories.index') }}"><i class="fa fa-circle-o"></i>List Category</a></li>
        <li><a href="{{ route('categories.create') }}"><i class="fa fa-plus"></i>Add Category</a></li>
    </ul>
</li>
<li class=" treeview">
    <a href="{{ route('articles.index') }}">
        <i class="fa fa-dashboard"></i> <span>Article</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class=""><a href="{{ route('articles.index') }}"><i class="fa fa-circle-o"></i>List Article</a></li>
        <li><a href="{{ route('articles.create') }}"><i class="fa fa-plus"></i>Add Article</a></li>
    </ul>
</li>
{{--
<li class="treeview">
    <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Layout Options</span>
        <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
        <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
        <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
        <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
    </ul>
</li>
--}}
