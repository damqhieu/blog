@extends('layouts.app-admin')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Article
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">eateData tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <a href="{{ route('articles.create') }}"><button class="btn btn-success">Add <i class="fa fa-plus"></i></button></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="datatables" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Title </th>
                                <th>Author</th>
                                <th>Content </th>
                                <th>Action  </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($articles as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item['title'] }}</td>
                                    <td>{{ $item['author'] }}</td>
                                    <td>{!! $item['content'] !!}</td>
                                    <td>
                                        <a href="{{ route('articles.edit',$item['id']) }}" class="btn btn-xs btn-info" title=""><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('articles.destroy',$item['id']) }}" class=" destroy btn btn-xs btn-danger" title=""><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('js')
    <script src="admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(function () {
            $('#datatables').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        })
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $('.destroy').click(function (e) {
            e.preventDefault();
            var _this = this;
            var url = $(this).attr('href');

            bootbox.confirm({
                title: "Destroy?",
                message: "Are you sure?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: url,
                            type: 'delete',
                            dataType: 'json',
                            success: function (rs) {
                                if (rs.status === 204) {
                                    $(_this).parents().eq(1).remove();
                                    bootbox.alert("Destroy success!");
                                } else {
                                    bootbox.alert("Destroy not success!");
                                }
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                alert("Status: " + textStatus); alert("Error: " + errorThrown);
                            }
                        })
                    }
                }
            });
        })
    </script>
@endsection
