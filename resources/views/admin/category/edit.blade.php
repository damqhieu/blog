@extends('layouts.app-admin')

@section('content')
    <section class="content-header">
        <h1>
            Category
            {{--<small>advanced tables</small>--}}
        </h1>
        {{--<ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">
                            {{--<small>Advanced and full of features</small>--}}
                        </h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"
                                    data-toggle="tooltip"
                                    title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                                    title="Remove">
                                <i class="fa fa-times"></i></button>

                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body pad">
                        <form action="{{ route('categories.update',$category->id) }}" method="post">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="id" value="{{ $category->id }}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Name:</label>
                                    <input type="text" name="name" value="{{ $category->name }}" id="name" class="form-control" onkeyup="generateSlug('name','slug')">
                                    @if ($errors->has('name'))
                                        <div class=" text-danger">
                                            {{  $errors->first('name') }}
                                        </div>
                                    @endif
                                </div>

                                <!-- Date range -->

                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="code">Slug:</label>
                                    <input type="text" class="form-control" value="{{ $category->slug }}" name="slug" id="slug">
                                    @if ($errors->has('slug'))
                                        <div class=" text-danger">
                                            {{  $errors->first('slug') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description:</label>
                                    <input type="text" name="description" value="{{ $category->description }}" class="form-control pull-right">
                                    @if ($errors->has('description'))
                                        <div class=" text-danger">
                                            {{  $errors->first('description') }}
                                        </div>
                                    @endif
                                <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="editor">Content</label>
                                    <textarea id="editor" name="content" rows="10" class="form-control">{{ $category->content }}</textarea>
                                    @if ($errors->has('content'))
                                        <div class=" text-danger">
                                            {{  $errors->first('content') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="box-footer text-center ">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="{{ route('categories.index') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('js')

    {{--editor--}}
    <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('editor'); // ID Tag
        })
    </script>
    <script>
        function generateSlug(inputStart, inputEnd) {
            var title;
            var slug;

            //Lấy text từ thẻ input title
            title = document.getElementById(inputStart).value;

            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();

            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*||∣|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '') + '.html';
            //In slug ra textbox có id “slug”
            document.getElementById(inputEnd).value = slug;
        }
    </script>
@endsection
