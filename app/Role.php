<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'slug', 'permissions'];

    protected $casts = ['permissions' => 'array'];

    public function users()
    {
        return $this->belongToMany(User::class);
    }

    public function hasAccess(array $permissions) : bool
    {
        foreach ($permissions as $permission ) {
            if ($this->hasPermission($permission))
                return true;
        }
        return false;
    }

    private function hasPermission(string $permission) : bool
    {
//        $data = $this->permissions;
//        foreach ($data as $key => $value){
//            if($key == $permission && $value == true){
//               return true;
//            }
//        }
//        return false;

        //So sanh mảng ở database có quyền này ko ?
        return $this->permissions[$permission] ?? false;
    }

}
