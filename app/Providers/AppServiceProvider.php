<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->registerCategoryRepository();
//        $this->registerArticleRepository();
    }

//    public function registerCategoryRepository() {
//        return $this->app->bind('App\Repositories\Contracts\CategoryRepositoryInterface', 'App\Repositories\Eloquents\CategoryRepository');
//    }
//
//    public function registerArticleRepository() {
//        return $this->app->bind('App\Repositories\Contracts\CategoryRepositoryInterface', 'App\Repositories\Eloquents\CategoryRepository');
//    }
}
