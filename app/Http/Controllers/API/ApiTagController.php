<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\TagRequest;
use App\Http\Resources\TagResource;
use App\Repositories\Eloquents\TagRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(TagRepository $tag)
    {
        $this->model = $tag;
    }


    public function index()
    {
        $this->authorize('tags.view');

        return TagResource::collection($this->model->getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $model = $this->model->store($request->all());
        $this->authorize('tags.view', $model);
        return new TagResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model  = $this->model->getById($id);
        $this->authorize('tags.view', $model);
        return new TagResource($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, $id)
    {
        $this->authorize('tags.update', $this->model->getById($id));
        return new TagResource($this->model->update($request->all(),$id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rs = $this->model->delete($id);
        $this->authorize('tags.delete', $rs);;
        return response()->json([], is_null($rs) ? 422 : 200);
    }
}
