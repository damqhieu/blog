<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Repositories\Eloquents\ArticleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(ArticleRepository $article)
    {
        $this->model = $article;
    }


    public function index()
    {
        $this->authorize('articles.view');

        return ArticleResource::collection($this->model->getAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $model = $this->model->store($request->all());
        $this->authorize('articles.view', $model);

        return new ArticleResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model  = $this->model->getById($id);
        $this->authorize('articles.view', $model);
        return new ArticleResource($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $this->authorize('articles.update', $this->model->getById($id));
        return new ArticleResource($this->model->update($request->all(),$id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rs = $this->model->delete($id);
        $this->authorize('articles.delete', $rs);
        return response()->json([], is_null($rs) ? 422 : 200);
    }
}
