<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'parent_id',
        'slug',
        'description',
        'content',
        'is_active'
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

}
