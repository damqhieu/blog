<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 12/09/2018
 * Time: 16:40
 */
namespace App\Repositories\Redis;

use App\Repositories\Contracts\CategoryRepositoryInterface;

class RedisCategoryRepository implements CategoryRepositoryInterface
{
    public function all()
    {
        return 'Get all category from Redis';
    }

    public function find($id)
    {
        return 'Get single category by id: ' . $id;
    }
}
