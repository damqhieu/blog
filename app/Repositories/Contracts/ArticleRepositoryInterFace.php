<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 13/09/2018
 * Time: 15:02
 */
namespace App\Repositories\Contracts;

use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;

interface ArticleRepositoryInterFace
{
    public function index();
    public function store(ArticleRequest $request);
    public function edit($id);
    public function update(ArticleRequest $request,$id);
    public function destroy($id);
}
