<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 12/09/2018
 * Time: 16:44
 */
namespace App\Repositories\Contracts;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

interface CategoryRepositoryInterface
{
    public function index();
    public function store(CategoryRequest $request);
    public function edit($id);
    public function update(CategoryRequest $request,$id);
    public function destroy($id);
}
