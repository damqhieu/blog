<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 18/09/2018
 * Time: 10:44
 */

namespace App\Repositories;


interface EntityInterface
{
    public function getAll();
    public function getById($id);
    public function store(array $data);
    public function update(array $data,$id);
    public function delete($id);
}
