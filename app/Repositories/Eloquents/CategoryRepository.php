<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 12/09/2018
 * Time: 16:33
 */

namespace App\Repositories\Eloquents;

use App\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{

    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

//    public function index()
//    {
//        return $categories = Category::cursor();
//    }
//
//
//    public function store(CategoryRequest $request)
//    {
//        $model = new Category();
//
//        $model->fill($request->all());
//
//        return $flag = $model->save();
//
//    }
//
//    public function edit($id)
//    {
//        return  Category::findOrFail($id);
//    }
//
//    public function update(CategoryRequest $request, $id)
//    {
//        $model = Category::findOrFail($id);
//
//        $model->fill($request->all());
//
//        return $flag = $model->update();
//
//    }
//
//    public function destroy($id)
//    {
//        $model = Category::findOrFail($id);
//
//        return $msg = $model->delete();
//
//    }
}
