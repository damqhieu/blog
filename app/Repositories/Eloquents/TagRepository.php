<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 12/09/2018
 * Time: 16:33
 */

namespace App\Repositories\Eloquents;

use App\Tag;
use App\Repositories\BaseRepository;

class TagRepository extends BaseRepository
{

    protected $model;

    public function __construct(Tag $tag)
    {
        $this->model = $tag;
    }

}