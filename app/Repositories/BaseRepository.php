<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 18/09/2018
 * Time: 10:47
 */
namespace App\Repositories;

abstract class BaseRepository implements EntityInterface
{

    protected $model;

    public function getAll(){
        return $this->model->all();
    }

    public function getById($id){
        return $this->model->find($id);
    }

    public function store(array $data){
        return $this->model->create($data);
    }

    public function update(array $data,$id){

       if($model = $this->getById($id)){
           $model->fill($data)->save();
           return $model;
       }
        return null;

    }

    public function delete($id){
        if($model = $this->getById($id)){
            $model->delete();
            return $model;
        }
        return null;
    }

}
